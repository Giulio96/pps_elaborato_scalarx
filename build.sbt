name := "ChainReaction"

version := "0.1"

scalaVersion := "2.13.7"


libraryDependencies ++= Seq(
  "com.lihaoyi" %% "scalarx" % "0.4.3",
  "junit" % "junit" % "4.11" % Test
)    