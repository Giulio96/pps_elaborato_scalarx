
import model.{Configuration, Direction, EAST, NORTH, Atom, SOUTH, WEST}
import org.junit._
import org.junit.Assert.{assertEquals, assertThat, assertTrue}
import utils.ListUtils._

class AtomsTest {

   @Test
   def testDirections(){
      val p1 = Atom(0,0)                                         //angolo basso-sx
      val p2 = Atom(0, Configuration.columns - 1)                       //angolo alto-sx
      val p3 = Atom(Configuration.rows - 1, 0)                    //angolo basso-dx
      val p4 = Atom(Configuration.rows - 1 , Configuration.columns - 1)   //angolo alto-dx

      val p5 = Atom(0, Configuration.columns  / 2)                   //bordo sx
      val p6 = Atom((Configuration.rows - 1) / 2, Configuration.columns - 1)  //bordo alto
      val p7 = Atom((Configuration.rows - 1) / 2, 0)                //bordo basso
      val p8 = Atom(Configuration.rows - 1, (Configuration.columns -1) / 2)  //bordo dx
      val p9 = Atom((Configuration.rows - 1) / 2, (Configuration.columns -1) / 2)  //centro
      val p10 = Atom(-1 , -1)                     //outside

      println(p2.directions)


      assertTrue(containsExactlyInAnyOrder(p1.directions)(List(NORTH, EAST)))
      assertTrue(containsExactlyInAnyOrder(p2.directions)(List(SOUTH, EAST)))
      assertTrue(containsExactlyInAnyOrder(p3.directions)(List(WEST, NORTH)))
      assertTrue(containsExactlyInAnyOrder(p4.directions)(List(SOUTH, WEST)))

      assertTrue(containsExactlyInAnyOrder(p5.directions)(List(NORTH, EAST, SOUTH)))
      assertTrue(containsExactlyInAnyOrder(p6.directions)(List(SOUTH, EAST, WEST)))
      assertTrue(containsExactlyInAnyOrder(p7.directions)(List(WEST, NORTH, EAST)))
      assertTrue(containsExactlyInAnyOrder(p8.directions)(List(SOUTH, WEST, NORTH)))

      assertTrue(containsExactlyInAnyOrder(p9.directions)(List(NORTH, SOUTH, WEST, EAST)))

      assertTrue(containsExactlyInAnyOrder(p10.directions) (List()))
   }
}
