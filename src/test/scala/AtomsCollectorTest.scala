import model.{AtomsCollector, Configuration, Atom, Grid}
import org.junit._
import org.junit.Assert.{assertEquals, assertThat, assertTrue}
import utils.ListUtils._
import utils.AppUtils._

class AtomsCollectorTest {

  @Test
  def testAtomsGeneration(){
    var collector = AtomsCollector.apply

    collector.placeAtomsInGrid(Configuration.columns - 1, Configuration.rows - 1)

    assertEquals(collector.getAtoms.size, Configuration.columns * Configuration.rows)

    for(a <- 0 to Configuration.columns - 1){
      for(b <- 0 to Configuration.rows - 1){
        val atomOpt : Option[Atom] = collector.getAtomInPosition(a,b);
        assertTrue(atomOpt.isDefined && atomOpt.get.xPos == a && atomOpt.get.yPos == b)
      }
    }

    assertTrue(collector.getAtomInPosition(-1,0).isEmpty)
    assertTrue(collector.getAtomInPosition(Configuration.columns,Configuration.rows).isEmpty)
    assertTrue(collector.getAtomInPosition(3,-1).isEmpty)

  }

  @Test
  def testAtomDependencies(): Unit ={
    var collector = AtomsCollector.apply
    collector.placeAtomsInGrid(Configuration.columns - 1, Configuration.rows - 1)
    collector.setAtomsDependencies()

    var a:Atom = collector.getAtomInPosition(0,0).get
    var a1:Atom = collector.getAtomInPosition(Configuration.columns/2, Configuration.rows/2).get
    var a2:Atom = collector.getAtomInPosition(Configuration.columns/2, 0).get


    assertTrue(a.getDependencies.size == a.directions.size)
    assertTrue(a1.getDependencies.size == a1.directions.size)
    assertTrue(a2.getDependencies.size == a2.directions.size)

    assertTrue(containsExactlyInAnyOrder(
      a.getDependencies.map(x => (x.xPos, x.yPos)))(
      a.directions.map(x => Grid.getCoordinates(a.xPos, a.yPos)(x)))
    )

    assertTrue(containsExactlyInAnyOrder(
      a1.getDependencies.map(x => (x.xPos, x.yPos)))(
      a1.directions.map(x => Grid.getCoordinates(a1.xPos, a1.yPos)(x)))
    )

    assertTrue(containsExactlyInAnyOrder(
      a2.getDependencies.map(x => (x.xPos, x.yPos)))(
      a2.directions.map(x => Grid.getCoordinates(a2.xPos, a2.yPos)(x)))
    )
  }

}
