package utils

import model.{AtomsCollector, Configuration}

object AppUtils {


  def printGrid(collector: AtomsCollector): Unit ={
    var grid:String = ""
    for(a <- 0 to Configuration.rows - 1){
      var line:String = ""
      for(b <- 0 to Configuration.columns -1){
        line += s" ${collector.getAtomInPosition(a,b).get.getAtomsCount} "
      }
      line += "\n"
      grid += line
    }

    println(grid)
  }
}
