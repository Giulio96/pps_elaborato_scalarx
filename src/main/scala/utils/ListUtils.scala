package utils

object ListUtils {

  def containsExactlyInAnyOrder[T](l1:List[T])(l2: List[T]):Boolean = {
    var listEquals = true
    if(l1.size == l2.size){
      l1.foreach(x => {
        if(!l2.contains(x)){
          listEquals = false
        }
      })
    }else{
      listEquals = false
    }
    listEquals
  }
}
