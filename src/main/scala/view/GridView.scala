package view

import model.{AppStateManager, Configuration}

import java.awt.{Color, Font, GridLayout}
import java.awt.event.{ActionEvent, ActionListener}
import javax.swing.{JButton, JFrame, JOptionPane, JPanel, WindowConstants}
import controller.SharedCtx

class GridView() {

  private var sharedCtx:SharedCtx = null
  private var cellsMap:Map[(Int, Int), JButton] = Map()
  private var frame = new JFrame("Chain reaction")

  def refreshValues(valueMap:Map[(Int, Int), Int]): Unit ={
    var b = valueMap
    valueMap.foreach(x => {
      var txt:String = if(x._2 > 0) s"${x._2.toString}" else ""
      this.cellsMap(x._1).setForeground(AppStateManager.getPlayerColor)
      this.cellsMap(x._1).setText(txt)
    })
  }

  def setSharedCtx(ctx:SharedCtx): Unit ={
    this.sharedCtx = ctx;
  }
  
  def buildGrid(): Unit ={
    var buttons:Set[JButton] = Set()
    val panel = new JPanel(new GridLayout(Configuration.rows, Configuration.columns))

    for(x <- 0 to Configuration.rows - 1) {
      for(y <- 0  to Configuration.columns - 1){
        var b = new JButton();
        b.setBackground(Color.DARK_GRAY)
        b.setFont(new Font("Arial", Font.PLAIN, 20))
        b.addActionListener(new ActionListener {
          override def actionPerformed(actionEvent: ActionEvent): Unit = {
            GridView.this.sharedCtx.onCellClick((x,y))
          }
        })
        this.cellsMap = this.cellsMap + (((x,y),b))
        panel.add(b)
      }
    }

    frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE)
    frame.setSize(Configuration.columns*Configuration.cellSize, Configuration.rows*Configuration.cellSize)
    frame.getContentPane.add(panel)
    frame.setVisible(true)
  }

  def onPlayersVictory(m:String): Unit ={
    JOptionPane.showMessageDialog(this.frame, m)
  }
  def getCellsMap:Map[(Int, Int), JButton] = this.cellsMap
}
