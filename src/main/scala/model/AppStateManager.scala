package model
import java.awt.Color

object AppStateManager {

  private var playerTurn = 1

  def nextTurn(): Unit ={
    playerTurn = if (playerTurn == Configuration.numPlayers) 1 else playerTurn + 1
  }

  def getTurn:Int = {
    playerTurn
  }

  def getColorOfPlayer(player:Int): Color ={
    Configuration.playerColors(player)
  }

  def getPlayerColor:Color = {
    Configuration.playerColors(playerTurn)
  }

}
