package model
import java.awt.Color

import scala.collection.mutable

object Configuration {

  val rows = 10
  val columns = 6
  
  val cellSize = 70
  
  val debug = false

  val numPlayers = 2

  val playerColors: mutable.Map[Int, Color] = mutable.HashMap(
    (1,Color.GREEN),
    (2, Color.ORANGE),
    (3,Color.CYAN),
    (4, Color.BLUE),
    (5,Color.YELLOW),
    (6, Color.PINK),
    (7,Color.MAGENTA),
    (8, Color.LIGHT_GRAY)
  )

}
