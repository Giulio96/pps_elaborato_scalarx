package model
import controller.SharedCtx
import rx._
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global


trait Atom {

  def xPos: Int
  def yPos: Int

  def directions:List[Direction]
  def getAtomsCount: Int
  def addParticle(turn:Int)
  def setDependencies(atoms:List[Atom])
  def getDependencies:List[Atom]
  def setSharedCtx(ctx:SharedCtx)
  def getPlayer:Option[Int]
}

object Atom{
  def apply (xPos: Int, yPos: Int): Atom = new AtomImpl(xPos, yPos)
}

case class AtomImpl(override val xPos:Int, override val yPos:Int) extends Atom{

  private val particlesCount = Var(0)
  private var atomsDependencies: List[Atom] = List()
  private var sharedCtx:Option[SharedCtx] = None
  private var player:Option[Int] = None
  implicit val ctx: Ctx.Owner = Ctx.Owner.safe()

  override def directions: List[Direction] = Grid.getDirections(xPos)(yPos)

  private var obsAdd = particlesCount.filter(_ <= this.directions.size)
  private var obsExp = particlesCount.filter(_ == this.directions.size)

  obsAdd.triggerLater{
    this.sharedCtx.get.updateValueInGrid(this.xPos, this.yPos)(particlesCount.now)(this.player.get)
  }

  this.initExplodeObs

  override def addParticle(turn:Int): Unit = this.synchronized{
      //println(s"adding particle in (${xPos},${yPos}) with turn ${turn}")
      this.player = Some(turn)
      this.particlesCount.update(_+1)
  }

  override def getAtomsCount: Int = {
    this.particlesCount.now
  }

  override def getPlayer: Option[Int] = {
    this.player
  }

  override def toString: String = {
    s"Atom(${this.xPos}, ${this.yPos})"
  }

  override def setDependencies(atoms: List[Atom]): Unit = this.atomsDependencies = atoms

  override def getDependencies: List[Atom] = this.atomsDependencies

  override def setSharedCtx(ctx: SharedCtx): Unit = {
    this.sharedCtx = Some(ctx)
  }

  private def initExplodeObs = {
    obsExp = particlesCount.filter(_ == this.directions.size)
    obsExp.triggerLater{
      Future{
        Thread.sleep(500)
        this.particlesCount.update(0)
      }.onComplete((_ => explode()))
    }
  }
  
  private def explode():Unit = {
    this.initExplodeObs
    this.atomsDependencies.foreach(x => {
      x.addParticle(this.player.get)
    })
    this.player = None
  }
}