package model

object Grid {

  def getDirections(xPos: Int)(yPos: Int): List[Direction] = {
    var directions: List[Direction] = List()

    if(canMove(xPos,yPos)(NORTH)){
      directions = directions :+ NORTH
    }

    if(canMove(xPos,yPos)(SOUTH)){
      directions = directions :+ SOUTH
    }

    if(canMove(xPos,yPos)(EAST)){
      directions = directions :+ EAST
    }

    if(canMove(xPos,yPos)(WEST)){
      directions = directions :+ WEST
    }


    if (Configuration.debug) println(directions)
    directions


  }
  
  def getCoordinates(actualPosition:(Int, Int))(d:Direction): (Int, Int) = d match {
    case NORTH => (actualPosition._1, actualPosition._2 + 1)
    case SOUTH => (actualPosition._1, actualPosition._2 - 1)
    case EAST => (actualPosition._1 + 1, actualPosition._2)
    case WEST => (actualPosition._1 - 1, actualPosition._2)

  }
  
  private def canMove(t:(Int, Int))(d:Direction) = {
    isInside(getCoordinates(t)(d))
  }

  private def isInside(t:(Int, Int)):Boolean = {
    t._1 >= 0 && t._1 < Configuration.rows && t._2 >= 0 && t._2 < Configuration.columns
  }

}


