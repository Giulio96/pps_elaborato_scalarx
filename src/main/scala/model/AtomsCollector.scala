package model

trait AtomsCollector{

  def placeAtomsInGrid(x:Int, y:Int):List[Atom]
  def getAtomInPosition(t:(Int, Int)):Option[Atom]
  def getAtoms:List[Atom]
  def setAtomsDependencies()

}

object AtomsCollector {

  def apply:AtomsCollector = new AtomsCollectorImpl()
}

case class AtomsCollectorImpl() extends AtomsCollector{

  private var atoms:List[Atom] = List()

  override def placeAtomsInGrid(x: Int, y: Int): List[Atom] = {
    for(a <- 0 to x) {
      for(b <- 0 to y){
        this.atoms = this.atoms :+ Atom(a,b)
      }
    }
    atoms
  }

  override def getAtomInPosition(t: (Int, Int)): Option[Atom] = {
    atoms.find(x => x.xPos == t._1 && x.yPos == t._2)
  }

  override def getAtoms: List[Atom] = atoms

  override def setAtomsDependencies(): Unit = {
     atoms.foreach(x => {
       var dependencies :List[Atom] = List()
       x.directions.foreach(d => {
         var atomOpt:Option[Atom] = getAtomInPosition(Grid.getCoordinates(x.xPos, x.yPos)(d))
         if(atomOpt.isDefined){
           dependencies = dependencies :+ atomOpt.get
         }
       })
       x.setDependencies(dependencies)
     })
  }

}