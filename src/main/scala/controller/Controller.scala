package controller

import model.{AppStateManager, Atom, AtomsCollector, Configuration}
import utils.AppUtils.printGrid
import view.GridView

class Controller {

  private var atomsMap:Map[(Int, Int), Atom] = Map()
  private var grid:GridView = null
  private var collector:AtomsCollector = AtomsCollector.apply
  private var sharedCtx:SharedCtx = SharedCtx.apply

  def setup(): Unit ={

    setupAtomsModel()
    this.atomsMap.values.foreach(x => x.setSharedCtx(this.sharedCtx))
    this.grid = new GridView()
    this.grid.buildGrid()
    this.sharedCtx.setButtonsMap(this.grid.getCellsMap)
    this.sharedCtx.setAtomsMap(this.atomsMap)
    this.grid.setSharedCtx(this.sharedCtx)
  }

  def onCellClick(t:(Int, Int)): Unit ={
    this.atomsMap.get(t._1, t._2).get.addParticle(AppStateManager.getTurn)
  }

  private def setupAtomsModel(): Unit ={
    collector.placeAtomsInGrid(Configuration.rows - 1, Configuration.columns - 1)
    collector.setAtomsDependencies()
    this.atomsMap = collector.getAtoms.map(x => ((x.xPos,x.yPos) -> x)).toMap
  }

}
