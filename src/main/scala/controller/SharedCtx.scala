package controller

import model.{AppStateManager, Atom,Configuration}
import javax.swing.JButton

trait SharedCtx{

  def setButtonsMap(map: Map[(Int,Int), JButton])

  def setAtomsMap(map: Map[(Int,Int), Atom])
  def updateValueInGrid(t:(Int,Int))(value:Int)(turn:Int)
  
  def onCellClick(t:(Int,Int))
}

object SharedCtx{
  def apply:SharedCtx = new SharedCtxImpl()
}

class SharedCtxImpl extends SharedCtx {

  private var buttonsMap:Map[(Int, Int), JButton] = Map()
  private var atomsMap:Map[(Int,Int), Atom] = Map()
  private var countTurn = 0
  private var win = false

  override def setButtonsMap(map: Map[(Int,Int), JButton]): Unit ={
    this.buttonsMap = map
  }

  override def setAtomsMap(map: Map[(Int, Int), Atom]): Unit = {
    this.atomsMap = map
  }

  override def updateValueInGrid(t:(Int,Int))(value:Int)(player:Int){
    this.synchronized{
      var txt:String = if(value > 0) s"${value.toString}" else ""
      this.buttonsMap(t).setForeground(AppStateManager.getColorOfPlayer(player))
      this.buttonsMap(t).setText(txt)
    }

    if(checkVictory()){
      println(s"Giocatore ${player} vince !!!");
      this.win = true;
    }

  }

  private def checkVictory(): Boolean ={
    if(this.countTurn > 2){
      var popAtoms = this.atomsMap.values.filter(a => a.getPlayer.isDefined)
      var partitionedPlayerAtoms = popAtoms.partition(a => a.getPlayer.get == 1)
      return partitionedPlayerAtoms._1.isEmpty || partitionedPlayerAtoms._2.isEmpty
    }
    return false
  }


  def printGrid(): Unit ={
    for( a <- 0 to Configuration.rows){
      for(b <- 0 to Configuration.columns - 1 ){
        print(atomsMap.get(a,b).get.getAtomsCount + "\t")
      }
      println()
    }
    println("\n\n")
  }
  override def onCellClick(t: (Int, Int)): Unit = {
    if(!this.win){
      var a : Atom = this.atomsMap(t)
      this.countTurn = this.countTurn + 1
      if(a.getPlayer.isEmpty || AppStateManager.getTurn == a.getPlayer.get){
        a.addParticle(AppStateManager.getTurn)
        AppStateManager.nextTurn()
      }
    }

  }

}
