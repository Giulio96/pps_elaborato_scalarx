package experiments

import rx._
trait MyRx[A] {
  def value:Var[A]
  def <+> (x:MyRx[A]):Rx.Dynamic[A]
  def <*> (x:MyRx[A]):Rx.Dynamic[A]
  def <= (v:A)
}


object MyRx {
  def apply(v:Int) = new MyRxImpl(Var(v))
}

case class MyRxImpl(override val value:Var[Int]) extends MyRx[Int]{
  implicit val ctx: Ctx.Owner = Ctx.Owner.safe()

  override def <+>(x:MyRx[Int]):Rx.Dynamic[Int] = {
    Rx{this.value() + x.value()}
  }

  override def <*>(x:MyRx[Int]):Rx.Dynamic[Int] = {
    Rx{this.value() * x.value()}
  }

  override def <= (v:Int) = {
    this.value() = v
  }
}

object TryMyRx extends App{

  var a = MyRx(2)
  var b = MyRx(3)

  var t = a <+> b
  var s = a <*> b

  println(t.now)
  println(s.now)


  a <= 8

  println(t.now)
  println(s.now)


}