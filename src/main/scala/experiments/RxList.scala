package experiments
import rx._
import rx.opmacros.Utils._
import utilities.Default

import scala.concurrent.{Await, Future, Promise}
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits.global


trait Person{
  def name:String
  def age:Int
  def sex:String

  def canEqual(that: Any): Boolean
}

object Person{
  def apply(name:String, age:Int, sex:String) = new PersonImpl(name,age,sex)
}

case class PersonImpl(override val name:String, override val age:Int, override val sex:String) extends Person{

  def canEqual(a: Any) = a.isInstanceOf[Person]

  override def equals(that: Any): Boolean =
    that match {
      case that: Person => {
        that.canEqual(this) &&
          this.name == that.name &&
          this.age == that.age
      }
      case _ => false
    }

  override def hashCode: Int = {
    val prime = 31
    var result = 1
    result = prime * result + age;
    result = prime * result + (if (name == null) 0 else name.hashCode)
    result
  }

}



object RxList {

  implicit class ListRx[A](base: List[A]) {

    implicit val ctx: Ctx.Owner = Ctx.Owner.safe()

    def filterRx(t: Id[A] => Boolean): List[A] = {
      var tmp: List[A] = List()
      var o = Var(Default.value[A])
      o.map(x => {
        if(x != null && t(x)){
          tmp = tmp :+ x
        }
      })

      for (y <- base){
        o() = y
        o() = Default.value[A]
      }
      tmp
    }

    def appendRx(item:A): List[A] = {
      val x = Var(item)
      x.fold(base)((acc,elem) => acc :+ elem).now
    }

    def appendAllRx(seq:A*):List[A] = {
      var l = base
      seq.foreach(x => {l = l.appendRx(x)})
      l
    }

    def prependRx(item:A):List[A] = {
      val x = Var(item)
      x.fold(base)((acc,elem) => elem :: acc).now
    }

    def prependAllRx(seq:A*):List[A] = {
      var l = base
      seq.foreach(x => {l = l.prependRx(x)})
      l
    }

    def reverseRx():List[A] = {
      var l:List[A] = List()
      base.foreach(x => {
        l = l.prependRx(x)
      })
      l
    }

    def tailRx():List[A] = {
      dropFirstRx(1)
    }

    def dropFirstRx(count:Int): List[A] = {
      var tmp:List[A] = List()
      var b = Var(count)
      var startTake = false

      val o = b.filter(_ == 0)
      o.triggerLater{
        startTake = true
      }
      for(y <- base){
        if(!startTake) {
          b() = b.now - 1
        } else
          tmp = tmp.appendRx(y)
      }
      tmp
    }

    def elementAtRx(index:Int) : A = {
      var tmp = dropFirstRx(index - 1)
      tmp.head
    }

    def removeRx(item:A):List[A] = {
      base.filterRx(_ != item)
    }

    def dropFromItemRx(item:A):List[A] = {
      var b = Var(0.asInstanceOf[A])
      val o = b.filter(_ == item)
      var take = true
      var tmp:List[A] = List()
      o.triggerLater{
        take = false
      }

      for(a <- base){
        if(take){
          tmp = tmp.appendRx(a)
        }
        Await.result(Future{b() = a}, Duration.Inf)
      }
      tmp
    }

    def takeFromItemRx(item:A):List[A] = {
      var b = Var(0.asInstanceOf[A])
      val o = b.filter(_ == item)
      var take = false
      var tmp:List[A] = List()
      o.triggerLater{
        take = true
      }

      for(a <- base){
        Await.result(Future{b() = a}, Duration.Inf)
        if(take){
          tmp = tmp.appendRx(a)
        }
      }
      tmp
    }

    def replaceRx(toReplace:A, item:A):List[A] = {
      var tmp:List[A] = List()
      def initObs():Var[A]= {
        var obs = Var(Default.value[A])
        obs.triggerLater{
          if(obs.now.equals(toReplace)) tmp = tmp.appendRx(item)
          else tmp = tmp.appendRx(obs.now)
        }
        obs
      }
      for(a <- base){
        var b = initObs()
        b() = a
      }
      tmp
    }

    def mapRx(f:(A) => A):List[A] = {
      val a = Var(0.asInstanceOf[A])
      var c = a.map(f)
      var tmp:List[A] = List()
      for(x <- base) {
        a() = x
        tmp = tmp.appendRx(c.now)
      }
      tmp

    }

    def reduceRx(f:(A,A) => A):A = {
      var a = Var(Default.value[A].asInstanceOf[A])
      var c = a.reduce(f)
      var tmp:List[A] = List()
      for(x <- base) {
        a() = x
      }
      c.now
    }

    def partitionRx(f:(A) => Boolean): (List[A], List[A]) = {
      (base.filterRx(f), base.filterRx(!f(_)))
    }

    def zipRx():List[(Int,A)] = {
      var a = Var(Default.value[A].asInstanceOf[A])
      var count:Int = 0
      var tmp:List[(Int,A)] = List()

      a.triggerLater{
        tmp = tmp.appendRx((count, a.now))
        count = count + 1
      }

      for(x <- base){
        a() = x
      }

      tmp
    }
  }



}


object TryRxList extends App{

  import RxList._

  println("----------TEST LIST INT----------")
  var li = List(1,1,1,2,2,3,3,0,8)
  println("FilterRx:")
  println(li.filterRx(_<2))
  println()

  println("AppendRx:")
  println(li.appendRx(13))
  println()

  println("AppendAllRx:")
  println(li.appendAllRx(50,51,52))
  println()

  println("PrependRx:")
  println(li.prependRx(-1))
  println()

  println("PrependAllRx:")
  println(li.prependAllRx(-3,-4,-5))
  println()

  println("ReverseRx:")
  println(li.reverseRx())
  println()

  println("TailRx:")
  println(li.tailRx())
  println()

  println("DropFirstRx:")
  println(li.dropFirstRx(5))
  println()

  println("ElementAtRx")
  println(li.elementAtRx(5))
  println()

  println("RemoveRx")
  println(li.removeRx(2))
  println()

  println("DropFromItemRx:")
  println(li.dropFromItemRx(3))
  println()

  println("TakeFromItemRx:")
  println(li.takeFromItemRx(1))
  println()

  println("ReplaceRx:")
  println(li.replaceRx(3,0))
  println()

  println("MapRx:")
  println(li.mapRx(_*4))
  println()

  println("ReduceRx:")
  println(li.reduceRx(_+_))
  println()

  println("PartitionRx:")
  println(li.partitionRx(_>3))
  println()

  println("ZipRx:")
  println(li.zipRx())
  println()
  println("-------------------------------------------------------")

  println("----------TEST LIST STRING----------")

  val ls:List[String]=List("abc","","AA", "e","giulio","elena", "andrea", "emy","ulu","ala")

  println("FilterRx:")
  println(ls.filterRx(_.contains("i")))
  println()

  println("AppendRx:")
  println(ls.appendRx("ciao"))
  println()

  println("AppendAllRx:")
  println(ls.appendAllRx("s1", "s2","s3"))
  println()

  println("PrependRx:")
  println(ls.prependRx("s0"))
  println()

  println("PrependAllRx:")
  println(ls.prependAllRx("s-3","s-4","s-5"))
  println()

  println("ReverseRx:")
  println(ls.reverseRx())
  println()

  println("TailRx:")
  println(ls.tailRx())
  println()

  println("DropFirstRx:")
  println(ls.dropFirstRx(5))
  println()

  println("ElementAtRx")
  println(ls.elementAtRx(5))
  println()

  println("RemoveRx")
  println(ls.removeRx("elena"))
  println()

  println("DropFromItemRx:")
  println(ls.dropFromItemRx("elena"))
  println()

  println("TakeFromItemRx:")
  println(ls.takeFromItemRx("elena"))
  println()

  println("ReplaceRx:")
  println(ls.replaceRx("elena","ELENA"))
  println()

  println("MapRx:")
  println(ls.mapRx(_*4))
  println()

  println("ReduceRx:")
  println(ls.reduceRx(_+_))
  println()

  println("PartitionRx:")
  println(ls.partitionRx(_.contains("i")))
  println()

  println("ZipRx:")
  println(ls.zipRx())
  println()
  println("-------------------------------------------------------")

  println("----------TEST LIST PERSON----------")

  var lp = List(
    Person("Giulio",24,"M"),
    Person("Carla",25,"F"),
    Person("Andrea", 55, "M"),
    Person("Monia",50,"F"),
    Person("Andrea", 55, "M"),
    Person("Monia",50,"F"),
    Person("Monia", 50, "F"),
    Person("Paolo",32,"M")
  )


  println("FilterRx:")
  println(lp.filterRx(_.age<30))
  println()

  println("AppendRx:")
  println(lp.appendRx(Person("Zio",47,"M")))
  println()

  println("AppendAllRx:")
  println(lp.appendAllRx(Person("Zio",47,"M"), Person("Zia",45,"F")))
  println()

  println("PrependRx:")
  println(lp.prependRx(Person("Zio",47,"M")))
  println()

  println("PrependAllRx:")
  println(lp.prependAllRx(Person("Zio",47,"M"),Person("Zia",45,"F")))
  println()

  println("ReverseRx:")
  println(lp.reverseRx())
  println()

  println("TailRx:")
  println(lp.tailRx())
  println()

  println("DropFirstRx:")
  println(lp.dropFirstRx(5))
  println()

  println("ElementAtRx")
  println(lp.elementAtRx(5))
  println()

  println("RemoveRx")
  println(lp.removeRx(Person("Carla",25,"F")))
  println()

  println("DropFromItemRx:")
  println(lp.dropFromItemRx(Person("Carla",25,"F")))
  println()

  println("TakeFromItemRx:")
  println(lp.takeFromItemRx(Person("Carla",25,"F")))
  println()

  println("ReplaceRx:")
  println(lp.replaceRx(Person("Carla",25,"F"),Person("Zio",47,"M")))
  println()

  println("MapRx:")
  println(lp.mapRx(x => Person(x.name, x.age + 30, x.sex)))
  println()

//  println("ReduceRx:")
//  println(ls.reduceRx(_+_))
//  println()

  println("PartitionRx:")
  println(lp.partitionRx(_.sex == ("F")))
  println()

  println("ZipRx:")
  println(lp.zipRx())
  println()
  println("-------------------------------------------------------")


}