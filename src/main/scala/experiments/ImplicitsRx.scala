package experiments

import rx._

trait Add[A,B,T] {
  def apply(a: A, b: B): T
}

object Add {

  implicit def rxAdd= new Add[Var[Int], Var[Int], Rx.Dynamic[Int]]{
    implicit val ctx: Ctx.Owner = Ctx.Owner.safe()
    def apply(a:Var[Int], b:Var[Int]): Rx.Dynamic[Int] = Rx{a() + b()}
  }

  implicit def rxDynamicAdd = new Add[Rx.Dynamic[Int], Var[Int], Rx.Dynamic[Int]]{
    implicit val ctx: Ctx.Owner = Ctx.Owner.safe()
    def apply(a:Rx.Dynamic[Int], b:Var[Int]): Rx.Dynamic[Int] = Rx{a() + b()}
  }
}


object RxTypesOp{
  implicit class RxInt(base:Var[Int]){
    def <+>(y: Var[Int]) : Rx.Dynamic[Int] = implicitly[Add[Var[Int],Var[Int], Rx.Dynamic[Int]]].apply(base, y)
  }

  implicit class RxDynamicInt(base:Rx.Dynamic[Int]){
    def <+>(y: Var[Int]) : Rx.Dynamic[Int] = implicitly[Add[Rx.Dynamic[Int],Var[Int], Rx.Dynamic[Int]]].apply(base, y)
  }
}

object ImplicitsRx extends App{

  import RxTypesOp._


  var a = Var(0)
  var a1 = Var(1)
  var a2 = Var(2)
  var a3 = Var(3)

  var res = a <+> a1 <+> a2 <+> a3



  println(res.now)

  a1() = 4

  println(res.now)


}
