package experiments.customization
import rx._
import experiments.utilities._

trait Grx[A] {
  def map(f: Var[A] => Var[A]): Grx[A]
  def flatMap(f: Var[A] => Grx[A]): Grx[A]
  def <+> (x:Grx[A])(implicit combining : RxCombinator[A]):Rx.Dynamic[A]
  def <*> (x:Grx[A])(implicit combining : RxNumericCombinator[A]):Rx.Dynamic[A]
}

object Grx {
  def apply[A](v:A) = new SomeGrx[A](Var(v))
}

case class SomeGrx[A](val value:Var[A]) extends Grx[A]{
  implicit val ctx: Ctx.Owner = Ctx.Owner.safe()

  override def map(f: Var[A] => Var[A]): Grx[A] = SomeGrx(f(this.value))

  override def flatMap(f: Var[A] => Grx[A]): Grx[A] = f(this.value)


  override def <+>(x:Grx[A])(implicit combiner : RxCombinator[A]):Rx.Dynamic[A] = x match {
    case SomeGrx(v) =>  combiner.combineSum(this.value, v)
    case NoneGrx() => Rx{this.value()}
  }

  override def <*>(x:Grx[A])(implicit combiner : RxNumericCombinator[A]):Rx.Dynamic[A] = x match {
    case SomeGrx(v)  => combiner.combineMul(this.value, v)
    case NoneGrx() => Rx{this.value()}
  }

  def <= (v:A) = {
    this.value() = v
  }
}

case class NoneGrx[A]() extends Grx[A]{
  implicit val ctx: Ctx.Owner = Ctx.Owner.safe()

  override def map(f: Var[A] => Var[A]): Grx[A] = NoneGrx()

  override def flatMap(f: Var[A] => Grx[A]): Grx[A] = NoneGrx()

  override def <+>(x:Grx[A])(implicit combiner : RxCombinator[A]):Rx.Dynamic[A] = x match {
    case SomeGrx(v) =>  Rx{v()}
    case _ => Rx{Default.value[A]}
  }

  override def <*>(x:Grx[A])(implicit combiner : RxNumericCombinator[A]):Rx.Dynamic[A] = x match {
    case SomeGrx(v)  => Rx{v()}
    case _ => Rx{Default.value[A]}
  }
}
object TryGrx extends App{
  implicit val implicitStringCombination : RxCombinator [ String ] = new StringCombinator
  implicit val implicitIntCombination : RxNumericCombinator [ Int ] = new IntCombinator
  implicit val implicitDoubleCombination : RxNumericCombinator [ Double ] = new DoubleCombinator

  var h = NoneGrx[Double]
  var st = Grx(1.0)
  var rh = h <*> st

  println(rh.now)     //1.0

  var a = Grx("ciao")
  var b = Grx(" mamma")

  var c = Grx(1)
  var d = Grx(2)


  var e = Grx(0.5)
  var f = Grx(2.8)

  var t = c <*> d
  println(t.now)

  c <= 5

  println(t.now)

  var res = a <+> b
  var res2 = c <+> d
  var res3 = e <+> f
  println(res.now)
  println(res2.now)

  d <= 4
  println(res2.now)

  println(res3.now)

  var myGrx = Grx(1)

  var ggg = for{
    a <- myGrx
    b <- NoneGrx[Int]
    c <- myGrx
  }yield {Var(a.now+b.now+c.now)}

  println(ggg)      //NoneGrx
}