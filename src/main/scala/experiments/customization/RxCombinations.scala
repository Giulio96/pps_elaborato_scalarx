package experiments.customization

import rx._


trait RxCombinator[A]{
  def combineSum(x: Var[A], y: Var[A]): Rx.Dynamic[A]
}

trait RxNumericCombinator[A] extends RxCombinator[A]{
  def combineMul(x: Var[A], y: Var[A]): Rx.Dynamic[A]
}

case class StringCombinator() extends RxCombinator[String]{
  implicit val ctx: Ctx.Owner = Ctx.Owner.safe()
  override def combineSum (x: Var[String] , y: Var[String] ) = Rx{x() + y()}
}

case class IntCombinator() extends RxNumericCombinator[Int]{
  implicit val ctx: Ctx.Owner = Ctx.Owner.safe()
  override def combineSum (x: Var[Int] , y: Var[Int] ) = Rx{x() + y()}
  override def combineMul(x: Var[Int], y: Var[Int]): Rx.Dynamic[Int] = { Rx{x() * y()} }
}

case class DoubleCombinator() extends RxNumericCombinator[Double]{
  implicit val ctx: Ctx.Owner = Ctx.Owner.safe()
  override def combineSum (x: Var[Double] , y: Var[Double] ) = Rx{x() + y()}
  override def combineMul(x: Var[Double], y: Var[Double]): Rx.Dynamic[Double] = {Rx{x() * y()}
  }
}

