package experiments
import rx._


object Nesting_tree extends App{
  var a = Var(0)
  var b = Var(0)
  var c = Var(0)
  var d = Var(0)

  var e = Rx{a() + b()}
  var f = Rx{c() + d()}

  var tree = Rx{e() + f()}

  a() = 1
  c() = 1
  println(tree.now)
}
