package experiments.utilities

import scala.collection.immutable


class Default[+A](val default: A)

trait DefaultImplicits {
  implicit def defaultNull[A <: AnyRef]:Default[A] = new Default[A](null.asInstanceOf[A])
}

object Default extends DefaultImplicits {
  implicit object DefaultDouble extends Default[Double](0.0)
  implicit object DefaultInt extends Default[Int](0)
  implicit object DefaultChar extends Default[Char]('\u0000')
  implicit object DefaultBoolean extends Default[Boolean](false)
  implicit object DefaultUnit extends Default[Unit](())
  implicit object DefaultString extends Default[String]("")

  def value[A](implicit value: Default[A]): A = value.default
}